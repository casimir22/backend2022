interface Producto {
    id:number;
    nombre:string;
    precio:number;
    stock:number;
}

//



const Producto1: Producto = {
    id:0,
    nombre: "Yogurt",
    precio: 100,
    stock: 13
};

const Producto2: Producto = {
    id:1,
    nombre: "Paquete de Galletas",
    precio: 89.9,
    stock: 40
};
const Producto3: Producto = {
    id:2,
    nombre: "Lata Coca-Cola",
    precio: 80.50,
    stock: 0
};
const Producto4: Producto = {
    id:3,
    nombre: "Alfajor",
    precio: 12,
    stock: 40
};

let productos: Array<Producto> = [
    Producto1,
    Producto2,
    Producto3,
    Producto4
]

console.log(productos);

export function getStock()
 {
   return productos;
 }

 export function storeProductos(body:any)
 {
   productos.push({
    id:body.id,
    nombre: body.nombre,
    stock: body.stock,
    precio: body.precio
   })
   console.log(body);
 }

 export function deleteProductos(indice:number)
 {
   productos.splice(indice)
 }
