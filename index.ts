// Importamos el paquete express
import express, { request } from 'express'
import * as productos from './productos'


// Instanciamos una app de express
const app = express()
app.use(express.json());



// Definimos una ruta y su handler correspondiente
app.get('/', function (request, response) {
    response.send('¡Bienvenidos a Express!')
})

app.get('/productos', function (request, response){
    response.send(productos.getStock())

})

app.post('/productos', function(request, response) {
    const body = request.body;
    productos.storeProductos(body);
    response.send("Agregaremos un producto a la lista")
})


app.delete('/productos/:id' , function(request, response) {
    const idProducto= Number(request.params.id);
    productos.deleteProductos(idProducto);
        response.send("Eliminaremos un producto de la lista")
}) 


// Ponemos a escuchar nuestra app de express
app.listen(3000, function () {
    console.info('Servidor escuchando en http://localhost:3000')
})

